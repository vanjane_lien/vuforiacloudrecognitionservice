/*===============================================================================
Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of QUALCOMM Incorporated, registered in the United States 
and other countries. Trademarks of QUALCOMM Incorporated are used with permission.
===============================================================================*/

#import <UIKit/UIKit.h>

//@class SampleAppLeftMenuViewController;

@interface SampleAppSlidingMenuController : UIViewController{
    
 
}

- (id)initWithRootViewController:(UIViewController*)controller;

- (void) showRootController:(BOOL)animated;


//- (void) dismiss;

@end

