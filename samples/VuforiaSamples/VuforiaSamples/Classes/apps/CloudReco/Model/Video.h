//
//  Video.h
//  VuforiaCloudRecognition
//
//  Created by Thuy Lien Nguyen on 10/3/14.
//  Copyright (c) 2014 Thuy Lien Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Video : NSObject
{
    NSString *targetID;
    NSString *thumbnailURL;
    NSString *title;
    NSString *videoURL;
}
@property (copy) NSString *videoURL;
@property (copy) NSString *title;
@property (copy) NSString *targetID;
@property (copy) NSString *thumbnailURL;


-(id)initWithDictionary:(NSDictionary *)aDictionary;

@end
