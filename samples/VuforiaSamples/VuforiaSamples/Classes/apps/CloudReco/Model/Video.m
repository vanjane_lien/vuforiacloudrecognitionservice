//
//  Video.m
//  VuforiaCloudRecognition
//
//  Created by Thuy Lien Nguyen on 10/3/14.
//  Copyright (c) 2014 Thuy Lien Nguyen. All rights reserved.
//

#import "Video.h"

@implementation Video
@synthesize thumbnailURL, targetID, title, videoURL;

#pragma mark - Public
- (id) initWithDictionary:(NSDictionary *)aDictionary
{
    self = [super init];
    if (self)
    {
        self.title = [aDictionary objectForKey:@"title"];
        self.thumbnailURL = [aDictionary objectForKey:@"thumbnail"];
        self.targetID = [aDictionary objectForKey:@"targetID"];
        self.videoURL = [aDictionary objectForKey:@"videoURL"];
    }
    
    return self;
}
@end
