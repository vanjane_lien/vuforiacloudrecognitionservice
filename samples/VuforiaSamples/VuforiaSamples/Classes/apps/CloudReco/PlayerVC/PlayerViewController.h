//
//  PlayerViewController.h
//  VuforiaCloudRecognition
//
//  Created by Thuy Lien Nguyen on 10/6/14.
//  Copyright (c) 2014 Thuy Lien Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "Video.h"

@interface PlayerViewController : UIViewController
{
    //MPMoviePlayerController * mpPlayer;
    AVPlayer *avplayer;
    AVPlayerLayer *avplayerlayer;
    
    Video *video;
    
    IBOutlet UIButton *btnDone;
    IBOutlet UIView *playerContainer;
}

-(id)initWithVideo:(Video *)aVideo;

@property (retain) Video *video;
//@property (retain) MPMoviePlayerController * mpPlayer;

- (IBAction)doneButtonTapped:(id)sender;


@end
