//
//  PlayerViewController.m
//  VuforiaCloudRecognition
//
//  Created by Thuy Lien Nguyen on 10/6/14.
//  Copyright (c) 2014 Thuy Lien Nguyen. All rights reserved.
//

#import "PlayerViewController.h"

@interface PlayerViewController ()

@end

@implementation PlayerViewController
@synthesize video;
//@synthesize mpPlayer;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[self playVideo];
    [self playVideoStream];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationChanged:)
                                                 name:@"UIDeviceOrientationDidChangeNotification"
                                               object:nil];
    //[btnDone setTitle:@"Done" forState:UIControlStateNormal];
    //[btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(id)initWithVideo:(Video *)aVideo{
    //self = [self initWithNibName:@"PlayerViewController" bundle:nil];
    self = [super init];
    if (self)
    {
        self.video = aVideo;
    }
    
    return self;
}



- (void)orientationChanged:(NSNotification *)notification{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    if (orientation == UIInterfaceOrientationPortrait)
    {
       
        
        CGRect viewBounds;
        viewBounds.origin.x = 0;
        viewBounds.origin.y = 0;
        viewBounds.size.width = self.view.frame.size.width;
        viewBounds.size.height = self.view.frame.size.height;
        
        [avplayerlayer setFrame:viewBounds];
    }
    /*else if (orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        
        CGRect viewBounds;
        viewBounds.origin.x = 0;
        viewBounds.origin.y = 0;
        viewBounds.size.width = self.view.frame.size.width;
        viewBounds.size.height = self.view.frame.size.height;
        
        [avplayerlayer setFrame:viewBounds];
    }*/
    else if (orientation == UIInterfaceOrientationLandscapeLeft)
    {
        CGRect viewBounds;
        viewBounds.origin.x = 0;
        viewBounds.origin.y = 0;
        viewBounds.size.width = self.view.frame.size.height;
        viewBounds.size.height = self.view.frame.size.width;
        
        [avplayerlayer setFrame:viewBounds];
    }
    else if (orientation == UIInterfaceOrientationLandscapeRight)
    {
      
        
        CGRect viewBounds;
        viewBounds.origin.x = 0;
        viewBounds.origin.y = 0;
        viewBounds.size.width = self.view.frame.size.height;
        viewBounds.size.height = self.view.frame.size.width;
        
        [avplayerlayer setFrame:viewBounds];
    }
    //do stuff
}



- (void)dealloc {
    [playerContainer release];
    [btnDone release];
    [super dealloc];
    [avplayerlayer removeFromSuperlayer];
    avplayerlayer = nil;
    [avplayer release];
    avplayer = nil;
}

- (void) playVideoStream {
    //CGRect screenRect = [[UIScreen mainScreen] bounds];
    NSURL *url = [NSURL URLWithString:[video valueForKey:@"videoURL"]];
    avplayer = [[AVPlayer alloc]initWithURL:url];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:avplayer];
    avplayerlayer =[AVPlayerLayer playerLayerWithPlayer:avplayer];
    [avplayerlayer setFrame:self.view.bounds];
    [playerContainer.layer addSublayer:avplayerlayer];
    avplayerlayer.videoGravity = AVLayerVideoGravityResizeAspect;
    [avplayer play];
}


-(void)itemDidFinishPlaying:(NSNotification *) notification {
    [[NSNotificationCenter defaultCenter]removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    
    [self doneButtonTapped:btnDone];
}

- (IBAction)doneButtonTapped:(id)sender
{
    //  Force closing overlay view with this notification
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kPlayerDismissed" object:nil];
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self dismissModalViewControllerAnimated:YES];
}
- (void)viewDidUnload {
    [playerContainer release];
    playerContainer = nil;
    [btnDone release];
    btnDone = nil;
    [super viewDidUnload];
}
@end
